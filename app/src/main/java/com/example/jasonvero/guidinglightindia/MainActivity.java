package com.example.jasonvero.guidinglightindia;


import android.Manifest;
import android.app.Activity;
import android.app.ActionBar;
import android.app.FragmentTransaction;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    HelperDialog helperDialog = new HelperDialog();
    ProgressDialog progressDialog;
    FirebaseAuth mAuth;

    private SectionsPageAdapter mSectionsPageAdapter;
    private ViewPager mViewPager;
    private LocationManager locationManager;
    private LocationListener locationListener;
    public static Double lat1, lng1;

    String coordinates;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mSectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());
        mAuth = FirebaseAuth.getInstance();

        // Set up the ViewPager with the sections adapter
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Getting your location.\nMake sure your GPS Location is enabled");
        progressDialog.setTitle("Initializing.");
        progressDialog.setCancelable(false);
        progressDialog.show();

        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                lat1 = location.getLatitude();
                lng1 = location.getLongitude();
                coordinates = "Lat: " + lat1 + "Lng: " + lng1;
                progressDialog.dismiss();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            return;
        }else{
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }


    }

    private void setupViewPager(ViewPager viewPager){
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new Tab1Fragment(), "POLICE DEPT.");
        adapter.addFragment(new Tab2Fragment(), "FIRE DEPT.");
        adapter.addFragment(new Tab3Fragment(), "MEDICAL SERVICES");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.signoutBtn){
            Toast.makeText(MainActivity.this, "Signing Out", Toast.LENGTH_SHORT).show();
            mAuth.signOut();
            Intent main = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(main);
        }else if(id == R.id.helperBtn){
//            Toast.makeText(MainActivity.this, "Helper Button", Toast.LENGTH_SHORT).show();
            openHelperDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    public void openHelperDialog(){
        helperDialog.setMessage("POLICE DEPARTMENT"+
                "\nHOSTAGE:\nselect this report when a hostage in your area is happeing" +
                        "\n\nDEAD BODY:\nselect this report when a dead body is in your area" +
                        "\n\nROBBERY:\nselect this report when a robbery is happening or a thief is in your area" +
                        "\n\nTROUBLE ALERT:\nselect this report when a riot or civilians causing trouble or a possible rape scenario\n"
                +"\n-------------------------------------------------------"
                +"\n-------------------------------------------------------" +
                "\n\nFIRE DEPARTMENT" + "\nMILD THREAT:\nselect this report when the fire has just begun or the fire is still small" +
                "\n\nWILD THREAT:\nselect this report when the fire is already huge" +
                "\n\nOUT OF CONTROL:\nselect this report the fire is already in a uncontrollable state or in a out of control state and please get to safety\n"
                +"\n-------------------------------------------------------"
                +"\n-------------------------------------------------------"
                +"\n\nMEDICAL SERVICES"
                +"\nGESTATING:\nselect this report when you or your wife is about to give birth" +
                "\n\nBONE FRACTURE:\nselect this report when you are suffering from a bone fracture" +
                "\n\nPOISONING:\nselect this report when you are currently being poisoned" +
                "\n\nMASSIVE BLEEDING:\nselect this report when you have a massive wound and losing blood fast");
        helperDialog.show(getSupportFragmentManager(), "HelpMePlease");
    }

    @Override
    public void finish() {
        super.finish();
    }
}
