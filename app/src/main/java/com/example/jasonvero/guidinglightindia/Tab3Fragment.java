package com.example.jasonvero.guidinglightindia;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.example.jasonvero.guidinglightindia.MainActivity.lat1;
import static com.example.jasonvero.guidinglightindia.MainActivity.lng1;

public class Tab3Fragment extends Fragment {

    private static final String TAG = "Tab3Fragment";
    DatabaseReference databaseReference;
    FirebaseAuth mAuth;
    String temp, chk;
    Button reportBtn;
    TextView textView;

    ImageView bonefracture, poisoning, pregnant, stabbydaddy;

    private SectionsPageAdapter mSectionsPageAdapter;
    private ViewPager mViewPager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.tab3_fragment, container, false);

        mAuth = FirebaseAuth.getInstance();
        final FirebaseUser fUser = mAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        reportBtn = view.findViewById(R.id.reportBtn);
        if(fUser.getEmail().trim() != ""){
            chk = "Registered User";
        }else{
            chk = "Guest User";
        }

        mSectionsPageAdapter = new SectionsPageAdapter(getActivity().getSupportFragmentManager());

        textView = view.findViewById(R.id.textView2);
        bonefracture = view.findViewById(R.id.bonefracture);
        poisoning = view.findViewById(R.id.poisoning);
        pregnant = view.findViewById(R.id.pregnant);
        stabbydaddy = view.findViewById(R.id.stabbydaddy);

        bonefracture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp = "bone fracture";
                textView.setText("Current Selection: " + temp);
            }
        });

        poisoning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp = "poisoning";
                textView.setText("Current Selection: " + temp);
            }
        });


        pregnant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp = "pregnant";
                textView.setText("Current Selection: " + temp);
            }
        });


        stabbydaddy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp = "Massive Bleeding";
                textView.setText("Current Selection: " + temp);
            }
        });



        reportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(temp!= null){
                    final String id = databaseReference.push().getKey();
                    IssueReport issueReport = new IssueReport(id, fUser.getUid().toString(), chk, "Police_Department_Category", temp);
                    databaseReference.child("IssueReport").child(id).setValue(issueReport).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(getActivity(), "Report was sent!", Toast.LENGTH_SHORT).show();
                                Coordinates coordinates =  new Coordinates(lat1.toString(), lng1.toString());
                                databaseReference.child("IssueReport").child(id).child("Location").setValue(coordinates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            mViewPager = (ViewPager) view.findViewById(R.id.container);
                                            setupViewPager(mViewPager);
                                        }
                                    }
                                });
                            }else{
                                Toast.makeText(getActivity(), "Sending Report Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    Toast.makeText(getActivity(), "Please make a selection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    private void setupViewPager(ViewPager viewPager){
        SectionsPageAdapter adapter = new SectionsPageAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new Tab1Fragment(), "POLICE DEPT.");
        viewPager.setAdapter(adapter);
    }
}
