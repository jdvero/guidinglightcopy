package com.example.jasonvero.guidinglightindia;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.w3c.dom.Text;

public class RegisterActivity extends AppCompatActivity {

    EditText emailText, passwordText, passwordTextBeta;
    Button nextBtn;
    String emailadd, passalpha, passbeta;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();
        emailText = findViewById(R.id.emailText);
        passwordText = findViewById(R.id.passwordText);
        passwordTextBeta = findViewById(R.id.passwordTextBeta);
        nextBtn = findViewById(R.id.nextButton);

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
//                Intent register2 = new Intent(RegisterActivity.this, RegisterActivity2.class);
//                startActivity(register2);
            }
        });
    }

    public void registerUser(){

        emailadd = emailText.getText().toString().trim();
        passalpha = passwordText.getText().toString().trim();
        passbeta = passwordTextBeta.getText().toString().trim();

        if(!TextUtils.isEmpty(emailadd) && !TextUtils.isEmpty(passalpha)  && !TextUtils.isEmpty(passbeta)){
            if(TextUtils.equals(passalpha, passbeta)){
                    mAuth.createUserWithEmailAndPassword(emailadd, passalpha).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(RegisterActivity.this, "Credentials Are Valid! Please Proceed", Toast.LENGTH_LONG).show();
                                Intent registerTwo = new Intent(RegisterActivity.this, RegisterActivity2.class);
                                startActivity(registerTwo);
                            }
                        }
                    });
            }else{
                Toast.makeText(RegisterActivity.this, "Your password does not match!", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(RegisterActivity.this, "Please input empty fields!", Toast.LENGTH_SHORT).show();
        }

    }
}
