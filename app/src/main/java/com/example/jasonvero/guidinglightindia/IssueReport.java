package com.example.jasonvero.guidinglightindia;

public class IssueReport {
    String report_ID;
    String user_ID;
    String user_type;
    String report_Category;
    String report_type;

    public IssueReport() {
        }

    public IssueReport(String report_ID, String user_ID, String user_type, String report_Category, String report_type) {
        this.report_ID = report_ID;
        this.user_ID = user_ID;
        this.user_type = user_type;
        this.report_Category = report_Category;
        this.report_type = report_type;
    }

    public String getReport_ID() {
        return report_ID;
    }

    public void setReport_ID(String report_ID) {
        this.report_ID = report_ID;
    }

    public String getUser_ID() {
        return user_ID;
    }

    public void setUser_ID(String user_ID) {
        this.user_ID = user_ID;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getReport_Category() {
        return report_Category;
    }

    public void setReport_Category(String report_Category) {
        this.report_Category = report_Category;
    }

    public String getReport_type() {
        return report_type;
    }

    public void setReport_type(String report_type) {
        this.report_type = report_type;
    }
}
