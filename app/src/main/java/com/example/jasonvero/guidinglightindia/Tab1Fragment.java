package com.example.jasonvero.guidinglightindia;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.example.jasonvero.guidinglightindia.MainActivity.lat1;
import static com.example.jasonvero.guidinglightindia.MainActivity.lng1;



public class Tab1Fragment extends Fragment {

    private static final String TAG = "Tab1Fragment";

    Button reportBtn;
    TextView textView, textView2;
    DatabaseReference databaseReference;
    FirebaseAuth mAuth;

    private SectionsPageAdapter mSectionsPageAdapter;
    private ViewPager mViewPager;
    ImageView hostage, riot, thief, deadbody;
    String temp, chk;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.tab1_fragment, container, false);

        mSectionsPageAdapter = new SectionsPageAdapter(getActivity().getSupportFragmentManager());

        mAuth = FirebaseAuth.getInstance();
        reportBtn = view.findViewById(R.id.reportBtn);
        final FirebaseUser fUser = mAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        textView = view.findViewById(R.id.textView2);
        textView2 = view.findViewById(R.id.textView17);

        hostage = view.findViewById(R.id.hostage);
        riot = view.findViewById(R.id.riot);
        thief = view.findViewById(R.id.thief);
        deadbody = view.findViewById(R.id.deadbody);
        if(fUser.getEmail().trim() != ""){
            chk = "Registered User";
        }else{
            chk = "Guest User";
        }

        hostage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp = "hostage";
                textView.setText("Current Selection: ");
                textView2.setText(temp);
            }
        });

        riot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp = "riot";
                textView.setText("Current Selection: ");
                textView2.setText(temp);
            }
        });

        thief.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp = "thief";
                textView.setText("Current Selection: ");
                textView2.setText(temp);
            }
        });

        deadbody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp = "dead body";
                textView.setText("Current Selection: ");
                textView2.setText(temp);
            }
        });


//        testBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getActivity(), "Latitude: " + lat1.toString() +"\n" + "Longitude: " + lng1.toString(), Toast.LENGTH_SHORT).show();
//                textView.setText("Latitude: " + lat1.toString() +"\n" + "Longitude: " + lng1.toString());
//            }
//        });

        reportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), chk, Toast.LENGTH_SHORT).show();
                if(temp!= null){
                    final String id = databaseReference.push().getKey();
                    IssueReport issueReport = new IssueReport(id, fUser.getUid().toString(), chk, "Police_Department_Category", temp);
                    databaseReference.child("IssueReport").child(id).setValue(issueReport).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(getActivity(), "Report was sent!", Toast.LENGTH_SHORT).show();
                                Coordinates coordinates =  new Coordinates(lat1.toString(), lng1.toString());
                                databaseReference.child("IssueReport").child(id).child("Location").setValue(coordinates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            mViewPager = (ViewPager) view.findViewById(R.id.container);
                                            setupViewPager(mViewPager);
                                        }
                                    }
                                });
                            }else{
                                Toast.makeText(getActivity(), "Sending Report Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    Toast.makeText(view.getContext(), "Please make a selection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    private void setupViewPager(ViewPager viewPager){
        SectionsPageAdapter adapter = new SectionsPageAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new Tab1Fragment(), "POLICE DEPT.");
        viewPager.setAdapter(adapter);
    }

}
