package com.example.jasonvero.guidinglightindia;

public class Coordinates {

    String curr_latitude;
    String curr_longitude;

    public Coordinates() {
    }

    public Coordinates(String curr_latitude, String curr_longitude) {
        this.curr_latitude = curr_latitude;
        this.curr_longitude = curr_longitude;
    }

    public String getCurr_latitude() {
        return curr_latitude;
    }

    public void setCurr_latitude(String curr_latitude) {
        this.curr_latitude = curr_latitude;
    }

    public String getCurr_longitude() {
        return curr_longitude;
    }

    public void setCurr_longitude(String curr_longitude) {
        this.curr_longitude = curr_longitude;
    }
}
