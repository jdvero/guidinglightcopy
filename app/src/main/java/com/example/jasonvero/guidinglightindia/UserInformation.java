package com.example.jasonvero.guidinglightindia;

public class UserInformation {

   String userID;
   String fName;
   String mName;
   String lName;
   String phoneNum;

    public UserInformation() {
    }

    public UserInformation(String userID, String fName, String mName, String lName, String phoneNum) {
        this.userID = userID;
        this.fName = fName;
        this.mName = mName;
        this.lName = lName;
        this.phoneNum = phoneNum;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }
}
