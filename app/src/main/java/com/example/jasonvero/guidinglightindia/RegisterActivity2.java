package com.example.jasonvero.guidinglightindia;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity2 extends AppCompatActivity {

    EditText firstName, middleName, lastName, phoneNumber;
    Button confirmBtn;
    private FirebaseAuth mAuth;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register2);

        firstName = findViewById(R.id.firstName);
        middleName = findViewById(R.id.middleName);
        lastName = findViewById(R.id.lastName);
        phoneNumber = findViewById(R.id.phoneNumber);
        confirmBtn = findViewById(R.id.confirmButton);

        mAuth =  FirebaseAuth.getInstance();


        databaseReference = FirebaseDatabase.getInstance().getReference("RegisteredUser");

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser2();
            }
        });
    }

    public void registerUser2(){

        FirebaseUser fUser = mAuth.getCurrentUser();

        String fname = firstName.getText().toString().trim();
        String mname = middleName.getText().toString().trim();
        String lname = lastName.getText().toString().trim();
        String number = phoneNumber.getText().toString().trim();

        if(!TextUtils.isEmpty(fname) && !TextUtils.isEmpty(mname) && !TextUtils.isEmpty(lname) && !TextUtils.isEmpty(lname) && !TextUtils.isEmpty(number)){
            UserInformation userInformation = new UserInformation(fUser.getUid().toString(), fname, mname, lname, number);
            databaseReference.child(fUser.getUid().toString()).setValue(userInformation).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(RegisterActivity2.this, "Successful Registration!", Toast.LENGTH_LONG).show();
                        Intent mainAct = new Intent(RegisterActivity2.this, MainActivity.class);
                        startActivity(mainAct);
                    } else {
                        Toast.makeText(RegisterActivity2.this, "Error!", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

    }
}
