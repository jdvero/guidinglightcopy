package com.example.jasonvero.guidinglightindia;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LoginActivity extends AppCompatActivity {

    EditText emailText, passwordText;
    Button loginBtn, registerBtn, asGuestBtn;
    String emailadd, password;
    private FirebaseAuth mAuth;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference("GuestUser");
        emailText = findViewById(R.id.emailText);
        passwordText = findViewById(R.id.passwordText);
        loginBtn = findViewById(R.id.loginBtn);
        registerBtn = findViewById(R.id.registerBtn);
        asGuestBtn = findViewById(R.id.loginAsGuestBtn);

        if(mAuth.getCurrentUser() != null){
            Intent main = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(main);
        }

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
//                Intent main = new Intent(LoginActivity.this, MainActivity.class);
//                startActivity(main);
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(register);
            }
        });

        asGuestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createGuest();
            }
        });
    }

    public void loginUser(){
        emailadd = emailText.getText().toString().trim();
        password = passwordText.getText().toString().trim();

        if(!TextUtils.isEmpty(emailadd) && !TextUtils.isEmpty(password)){
            mAuth.signInWithEmailAndPassword(emailadd, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        Intent main = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(main);
                    }else{
                        Toast.makeText(LoginActivity.this, "Please enter correct email address and password!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else{
            Toast.makeText(LoginActivity.this, "Please enter your email address and password!", Toast.LENGTH_SHORT).show();
        }
    }

    public void createGuest(){
        final String id = databaseReference.push().getKey();
        mAuth.signInAnonymously().addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    FirebaseUser fUser = mAuth.getCurrentUser();
                    UserInformation userInformation = new UserInformation(id, "GuestUser", null, null, null);
                    databaseReference.child(id).setValue(userInformation);
                    Toast.makeText(LoginActivity.this, "Successfully Logged In As Guest!", Toast.LENGTH_LONG).show();
                    Intent mainAct = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(mainAct);
                }else{
                    Toast.makeText(LoginActivity.this, "Error!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}
