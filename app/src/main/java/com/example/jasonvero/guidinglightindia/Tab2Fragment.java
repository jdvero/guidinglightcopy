package com.example.jasonvero.guidinglightindia;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.example.jasonvero.guidinglightindia.MainActivity.lat1;
import static com.example.jasonvero.guidinglightindia.MainActivity.lng1;

public class Tab2Fragment extends Fragment {

    private static final String TAG = "Tab2Fragment";

    Button reportBtn;
    TextView textView;

    DatabaseReference databaseReference;
    FirebaseAuth mAuth;
    String temp, chk;

    ImageView fireone, firetwo, firethree;

    private SectionsPageAdapter mSectionsPageAdapter;
    private ViewPager mViewPager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.tab2_fragment, container, false);

        reportBtn = view.findViewById(R.id.reportBtn);
        mAuth = FirebaseAuth.getInstance();
        final FirebaseUser fUser = mAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        fireone = view.findViewById(R.id.fireone);
        firetwo = view.findViewById(R.id.firetwo);
        firethree = view.findViewById(R.id.firethree);

        if(fUser.getEmail().trim() != ""){
            chk = "Registered User";
        }else{
            chk = "Guest User";
        }
        mSectionsPageAdapter = new SectionsPageAdapter(getActivity().getSupportFragmentManager());

        textView = view.findViewById(R.id.textView2);

        fireone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp = "fire one";
                textView.setText("Current Selection: " + temp);
            }
        });

        firetwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp = "fire two";
                textView.setText("Current Selection: " + temp);
            }
        });

        firethree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp = "fire three";
                textView.setText("Current Selection: " + temp);
            }
        });


        reportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(temp!= null){
                    final String id = databaseReference.push().getKey();
                    IssueReport issueReport = new IssueReport(id, fUser.getUid().toString(), chk, "Police_Department_Category", temp);
                    databaseReference.child("IssueReport").child(id).setValue(issueReport).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(getActivity(), "Report was sent!", Toast.LENGTH_SHORT).show();
                                Coordinates coordinates =  new Coordinates(lat1.toString(), lng1.toString());
                                databaseReference.child("IssueReport").child(id).child("Location").setValue(coordinates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            mViewPager = (ViewPager) view.findViewById(R.id.container);
                                            setupViewPager(mViewPager);
                                        }
                                    }
                                });
                            }else{
                                Toast.makeText(getActivity(), "Sending Report Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    Toast.makeText(getContext(), "Please make a selection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    private void setupViewPager(ViewPager viewPager){
        SectionsPageAdapter adapter = new SectionsPageAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new Tab1Fragment(), "POLICE DEPT.");
        viewPager.setAdapter(adapter);
    }

}
